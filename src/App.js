import React, { Component } from "react";
import "./App.css";
import TodoForm from "./TodoForm";
import TodoList from "./TodoList";

class App extends Component {
  constructor() {
    super();
    this.state = {
      todoAll: []
    };
  }

  addItem = name => {
    this.setState({
      todoAll: [].concat(this.state.todoAll).concat([{name, checked: false}])
    });
  };

  deleteItem = (index) => {
    let arr = [...this.state.todoAll]
    arr.splice(index, 1)
    this.setState({
      todoAll: arr
    })
  };

  checkItem = (index) => {
    let arr = [...this.state.todoAll]
    arr[index].checked = arr[index].checked ? false : true
    this.setState({
      todoAll: arr
    })
  };

  render() {
    return (
      <div>
        <TodoForm onSubmit={this.addItem} />
        <TodoList
          items={this.state.todoAll}
          deleteHandler={this.deleteItem}
          checkHandler={this.checkItem}
        />
      </div>
    );
  }
}

export default App;
