import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

import TodoForm from "./TodoForm";
import TodoList from "./TodoList";

import ShallowRenderer from "react-test-renderer/shallow";

describe("Renders without crashing", () => {
  it("App container", () => {
    const div = document.createElement("div");
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
});

describe("Should Shallow render: ", () => {
  const renderer = new ShallowRenderer();

  it("form", () => {
    const result = renderer.render(<TodoForm />);
    expect(result).toMatchSnapshot();
  });

  it("list", () => {
    const result = renderer.render(<TodoList />);
    expect(result).toMatchSnapshot();
  });
});

import TestRenderer from "react-test-renderer";

describe("App container", () => {
  let todoApp;
  let todoForm;

  it("initialize with an empty list", () => {
    todoApp = TestRenderer.create(<App />);
    expect(todoApp.getInstance().state.todoAll).toEqual([]);
  });

  it("adds item to the list", () => {
    todoApp = TestRenderer.create(<App />);
    todoApp.getInstance().addItem("testAdd");
    expect(todoApp.getInstance().state.todoAll).toEqual([
      { name: "testAdd", checked: false }
    ]);
  });

  it("passes addItem to Form", () => {
    todoApp = TestRenderer.create(<App />);
    todoForm = todoApp.root.findByType(TodoForm);
    const addItem = todoApp.getInstance().addItem;
    expect(todoForm.props.onSubmit).toEqual(addItem);
  });

  it("form receives addItem as prop", () => {
    todoApp = TestRenderer.create(<App />);
    todoForm = todoApp.root.findByType(TodoForm);
    todoForm.props.onSubmit("hi");
    expect(todoApp.getInstance().state.todoAll).toEqual([
      { checked: false, name: "hi" }
    ]);
  });

  it("should delete items", () => {
    todoApp = TestRenderer.create(<App />);
    todoApp.getInstance().addItem("testAdd");
    todoApp.getInstance().deleteItem(0);
    expect(todoApp.getInstance().state.todoAll).toEqual([]);
  });
});
