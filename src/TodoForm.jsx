import React from "react";
import PropTypes from "prop-types";

export default class TodoForm extends React.Component {
  constructor() {
    super();
    this.state = {
      text: ""
    };
  }

  handleChange = event => {
    this.setState({
      text: event.target.value
    });
  };

  handleKeyPress = event => {
    if (event.key === "Enter") {
      this.props.onSubmit(this.state.text);
      this.setState({
        text: ""
      })
    }
  };

  handleClick = () => {
    this.props.onSubmit(this.state.text);
    this.setState({
      text: ""
    })
  };

  render() {
    return (
      <div>
        <input
          type="text"
          onChange={this.handleChange}
          onKeyPress={this.handleKeyPress}
          value={this.state.text}
        />
        <button type="submit" onClick={this.handleClick}>
          Add
        </button>
      </div>
    );
  }
}

TodoForm.propTypes = {
  onSubmit: PropTypes.func
};
