import React from "react";

export default class TodoList extends React.Component {
  handleCheck = i => {
    this.props.checkHandler(i);
  };

  handleDelete = i => {
    this.props.deleteHandler(i);
  };

  render() {
    return this.props.items ? (
      <ul>
        {this.props.items.map((obj, index) => (
          <li key={index}>
            {
              <input
                type="checkbox"
                checked={obj.checked}
                onClick={event => this.handleCheck(index)}
              />
            }
            <span
              style={{ textDecoration: obj.checked ? "line-through" : "none" }}
            >
              {obj.name}
            </span>
            {
              <input
                type="button"
                value="x"
                onClick={event => this.handleDelete(index)}
              />
            }
          </li>
        ))}
      </ul>
    ) : null;
  }
}
