import React from "react";
import TestRenderer from "react-test-renderer";

import TodoForm from "./TodoForm";

import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });

import { spy } from "sinon";

describe("Input form", () => {
  const formReact = TestRenderer.create(<TodoForm />);

  it("should contain an input area", () => {
    expect(formReact.root.findAllByType("input")).toHaveLength(1);
  });

  it("should contain a button", () => {
    expect(formReact.root.findAllByType("button")).toHaveLength(1);
  });

  it("should accept input", () => {
    const formEnzyme = shallow(<TodoForm />);
    formEnzyme.find("input").simulate("change", {
      target: { value: "monkey" }
    });
    expect(formEnzyme.state().text).toEqual("monkey");
  });

  it("should call onSubmit when returned", () => {
    const inputSpy = spy();
    const formEnzyme = shallow(<TodoForm onSubmit={inputSpy} />);
    const input = formEnzyme.find("input");
    input.simulate("keyPress", {
      key: "Enter",
      target: { value: "monkey" }
    });
    expect(inputSpy.called).toBe(true);
  });

  it("should call onSubmit when Add is clicked", () => {
    const buttonSpy = spy();
    const formEnzyme = shallow(<TodoForm onSubmit={buttonSpy} />);
    formEnzyme.setState({ text: "monkey" });
    const button = formEnzyme.find("button");
    button.simulate("click");
    expect(buttonSpy.called).toBe(true);
  });
});
