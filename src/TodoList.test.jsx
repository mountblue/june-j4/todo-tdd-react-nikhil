import React from "react";

import TodoList from "./TodoList";

import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
configure({ adapter: new Adapter() });

import { spy } from "sinon";

describe("Todo List", () => {
  it("should render when state is empty", () => {
    const listEnzyme = shallow(<TodoList items={[]} />);
    expect(listEnzyme.find("li")).toHaveLength(0);
  });

  it("should render some items", () => {
    const todoAll = ["barry", "monkey", "allen"];
    const listEnzyme = shallow(<TodoList items={todoAll} />);
    expect(listEnzyme.find("li")).toHaveLength(3);
  });

  it("should have a checkbox", () => {
    const todoAll = ["barry", "monkey", "allen"];
    const listEnzyme = shallow(<TodoList items={todoAll} />);
    expect(listEnzyme.find({ type: "checkbox" })).toHaveLength(3);
  });

  it("should have a delete button", () => {
    const todoAll = ["barry", "monkey", "allen"];
    const listEnzyme = shallow(<TodoList items={todoAll} />);
    expect(listEnzyme.find({ type: "button" })).toHaveLength(3);
  });
});

describe("checkbox", () => {
  it("should allow clicking", () => {
    const checkSpy = spy();
    const listEnzyme = shallow(
      <TodoList items={["D"]} checkHandler={checkSpy} />
    );
    listEnzyme.find({ type: "checkbox" }).simulate("click");
    expect(checkSpy.calledOnce).toEqual(true);
  });
});

describe("delete", () => {
  it("should allow clicking", () => {
    const delSpy = spy();
    const listEnzyme = shallow(
      <TodoList items={["D"]} deleteHandler={delSpy} />
    );
    listEnzyme.find({ type: "button" }).simulate("click");
    expect(delSpy.calledOnce).toEqual(true);
  });
});
